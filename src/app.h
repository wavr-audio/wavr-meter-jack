#pragma once
#include <cassert>
#include <iostream>
#include <string>

#include <gtkmm/application.h>
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <jack/jack.h>
#include <jack/types.h>

#include <gtkmm/window.h>

#include "meter.h"

namespace Wavr {
class JackApplication : public Gtk::Application {
public:
    static Glib::RefPtr<JackApplication> create() { return Glib::RefPtr<JackApplication>(new JackApplication()); }
    int run() { return Gtk::Application::run(m_win); }

    void setup_signals(Glib::RefPtr<JackApplication>) { // TODO: Setup signals
#if 0
        auto sig_cb = std::mem_fun(app, &JackApplication::recieve_signal);
        signal(SIGQUIT, signal_handler);
        signal(SIGTERM, signal_handler);
        signal(SIGHUP, signal_handler);
        signal(SIGINT, signal_handler);
#else
        std::cout << "TODO: Set up signals" << std::endl;
#endif
    }

protected:
    enum MessageType { MESSAGE_NONE, MESSAGE_PORT_UNREGISTER, MESSAGE_PORT_REGISTER, MESSAGE__SIZE };
    JackApplication()
        : Gtk::Application("com.gitlab.Wavr.WavrJack"), m_client_name("Wavr Jack Meter"), m_meter(2),
          m_box(Gtk::Orientation::ORIENTATION_VERTICAL), m_btn_settings("_Settings", true), m_dispatcher(), m_mutex() {
        m_btn_settings.set_relief(Gtk::ReliefStyle::RELIEF_NONE);
        m_dispatcher.connect(sigc::mem_fun(*this, &JackApplication::dispatcher_callback));
    }

    void on_activate() override {
        jack_activate();
        create_window();
    }
    void jack_activate() {
        jack_options_t jackopts = JackNullOption;
        jack_status_t jack_status;

        m_client = jack_client_open(m_client_name.c_str(), jackopts, &jack_status);
        if (m_client == NULL) {
            std::cerr << "Couldn't open JACK client. Status " << jack_status;
            if (jack_status & JackServerFailed)
                std::cerr << ": Couldn't connect to server.";
            std::cerr << std::endl;
            exit(1);
        }
        if (jack_status & JackServerStarted) {
            std::cout << "JACK server started." << std::endl;
        }
        if (jack_status & JackNameNotUnique) {
            m_client_name.assign(jack_get_client_name(m_client));
            std::cout << "JACK: Unique name " << m_client_name << " assigned." << std::endl;
        }

        jack_set_process_callback(m_client, JackApplication::process, this);
        jack_set_port_connect_callback(m_client, JackApplication::port_callback, this);
        jack_on_shutdown(m_client, JackApplication::jack_shutdown, this);

        register_port(true);
        register_port(true);

        if (::jack_activate(m_client)) {
            std::cerr << "JACK: Couldn't activate client" << std::endl;
            exit(1);
        }

        auto ports = jack_get_ports(m_client, NULL, NULL, JackPortIsInput | JackPortIsPhysical);
        if (ports == NULL)
            std::cerr << "JACK: No inputs" << std::endl;
        else {
            jack_connect(m_client, jack_port_name(m_ports[0]), ports[0]);
            jack_connect(m_client, jack_port_name(m_ports[1]), ports[1]);
        }
    }
    void create_window() {
        g_object_set(gtk_settings_get_default(), "gtk-application-prefer-dark-theme", true, NULL);

        m_win.add(m_box);
        m_win.set_border_width(5);
        m_box.pack_start(m_meter);
        m_box.pack_start(m_btn_settings, Gtk::PackOptions::PACK_SHRINK);
        m_box.set_spacing(5);

        m_box.show_all();
    }

    void recieve_signal(int) { quit(); }

    bool is_port_connected(size_t i) {
        auto connections = jack_port_get_connections(m_ports[i]);
        if (connections != NULL) {
            jack_free(connections);
            return true;
        }
        return false;
    }

    int get_connected_ports() {
        int n = 0;
        for (size_t i = 0; i < m_ports.size(); i++) {
            if (is_port_connected(i))
                n++;
        }

        return n;
    }

private:
    static int process(jack_nframes_t nframes, void *instance) {
        JackApplication *self = (JackApplication *)instance;
        std::vector<std::vector<float>> data(self->get_connected_ports());
        size_t c = 0;
        for (size_t i = 0; i < self->m_ports.size(); i++) {
            if (!self->is_port_connected(i))
                continue;
            jack_default_audio_sample_t *sample =
                (jack_default_audio_sample_t *)jack_port_get_buffer(self->m_ports[i], nframes);
            data[c].resize(nframes);
            std::copy(sample, sample + nframes, data[c].begin());
            c++;
        }
        self->m_meter.process(data);

        return 0;
    }

    static void jack_shutdown(void *instance) { ((JackApplication *)instance)->quit(); }
    static void port_callback(jack_port_id_t, jack_port_id_t, int connect, void *arg) {
        JackApplication *self = (JackApplication *)arg;
        if (connect) {
            self->dispatch(MESSAGE_PORT_REGISTER);
            return;
        }
        self->dispatch(MESSAGE_PORT_UNREGISTER);
    }
    void register_port(bool force = false) {
        if (!force && get_connected_ports() < 2)
            return;
        std::ostringstream name;
        name << "Input " << m_ports.size();
        jack_port_t *new_port =
            jack_port_register(m_client, name.str().c_str(), JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
        if (new_port == NULL)
            std::cerr << "JACK: No more ports available." << std::endl;
        else
            m_ports.push_back(new_port);
    }
    void remove_last_ports() {
        if (!is_port_connected(m_ports.size() - 1)) {
            jack_port_unregister(m_client, m_ports[m_ports.size() - 1]);
            m_ports.erase(m_ports.end() - 1);
        }
    }
    void unregister_port() {
        if (m_ports.size() > 2) {
            remove_last_ports();
        }
    }
    void dispatcher_callback() {
        std::lock_guard<std::mutex> lock(m_mutex);
        std::cout << "Receiving message " << m_message << std::endl;
        switch (m_message) {
        case MESSAGE_PORT_REGISTER:
            register_port();
            break;
        case MESSAGE_PORT_UNREGISTER:
            unregister_port();
            break;
        default:
            break;
        }
    }
    void dispatch(MessageType type) {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_message = type;
        m_dispatcher.emit();
    }

    jack_client_t *m_client;
    std::string m_client_name;

    std::vector<jack_port_t *> m_ports;
    Meter m_meter;
    Gtk::Window m_win;
    Gtk::Box m_box;
    Gtk::Button m_btn_settings;
    Glib::Dispatcher m_dispatcher;
    MessageType m_message;
    mutable std::mutex m_mutex;
};
} // namespace Wavr
