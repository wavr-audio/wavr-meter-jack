#include <iostream>
#include <vector>

#include <jack/jack.h>

#include "app.h"
#define PROJECT_NAME "wavr-jack-meter"

int main(int argc, char **argv) {
    int jack_M, jack_m, jack_p, jack_b;
    Glib::RefPtr<Wavr::JackApplication> app;
    if (argc != 1) {
        std::cout << argv[0] << "takes no arguments.\n";
        return 1;
    }
    jack_get_version(&jack_M, &jack_m, &jack_p, &jack_b);
    std::cout << "This is project " << PROJECT_NAME << ", using Jack v" << jack_M << "." << jack_m << "." << jack_p
              << std::endl;
    app = Wavr::JackApplication::create();

    return app->run();
}
