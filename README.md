# Wavr Meter for JACK

A small audio meter for JACK audio - used as a real-life testbed for the meter widget.

## Features

- **Dynamic number of connections**: Checks connected ports and add/remove as needed - theoretically supports unlimited number of ports.
- **User settings** (TODO): Configurable dB range and falloff speed for the meter
- **Multiple modes** (TODO): PPM and EBU modes of metering.